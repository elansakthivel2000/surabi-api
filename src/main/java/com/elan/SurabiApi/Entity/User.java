package com.elan.SurabiApi.Entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

/*
 * 	User Entity
 */

@Entity
@Table(name = "users")
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	@Column(name = "username")
	private String username;
	
	// for not returning password in json (only accepting password field in json)
	@JsonProperty(access = Access.WRITE_ONLY)
	@Column(name = "password")
	private String password;
	
	@Column(name = "role")
	private String role;
	
	@Column(name = "is_logged_in")
	private int isLoggedIn;
	
	// to avoid recursive printing of json
	@JsonManagedReference
	@OneToMany(
		mappedBy = "purchasedBy",
		cascade = {
			CascadeType.PERSIST,
			CascadeType.REFRESH,
			CascadeType.DETACH,
			CascadeType.MERGE
		},
		fetch = FetchType.LAZY
	)
	private List<Bill> purchases;
	
	public User() {
		role = "USER";
		isLoggedIn = 1;
	}

	public User(String username, String password, String role) {
		this.username = username;
		this.password = password;
		this.role = role;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	// to avoid sending password in response
	@JsonIgnore
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public int isLoggedIn() {
		return isLoggedIn;
	}

	public void setLoggedIn(int isLoggedIn) {
		this.isLoggedIn = isLoggedIn;
	}

	public List<Bill> getPurchases() {
		return purchases;
	}

	public void setPurchases(List<Bill> purchases) {
		this.purchases = purchases;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", password=" + password + ", role=" + role
				+ ", isLoggedIn=" + (isLoggedIn == 0 ? "false" : "true") + "]";
	}
}
