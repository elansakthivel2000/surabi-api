package com.elan.SurabiApi.Entity;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

/*
 * 	Bill Entity
 */

@Entity
@Table(name="bills")
public class Bill {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="bill_amount")
	private double billAmount;
	
	@Column(name="num_items")
	private int numItems;
	
	@Column(name = "purchased_at")
	private Timestamp purchasedAt;
	
	// to avoid recursive printing of json
	@JsonBackReference
	@ManyToOne(
		cascade = {
			CascadeType.PERSIST,
			CascadeType.REFRESH,
			CascadeType.DETACH,
			CascadeType.MERGE
		},
		fetch = FetchType.EAGER
	)
	@JoinColumn(name = "purchased_by")
	private User purchasedBy;
	
	// to avoid recursive printing of json
	@JsonManagedReference
	@OneToMany(
		mappedBy = "bill",
		cascade = CascadeType.ALL,
		fetch = FetchType.LAZY
	)
	private List<Order> orders;

	public Bill() {
		purchasedAt = new Timestamp(System.currentTimeMillis() + (((5 * 60) + 30) * 60 * 1000));
	}
	
	public Bill(double billAmount, int numItems, User purchasedBy) {
		this.billAmount = billAmount;
		this.numItems = numItems;
		this.purchasedBy = purchasedBy;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getBillAmount() {
		return billAmount;
	}

	public void setBillAmount(double billAmount) {
		this.billAmount = billAmount;
	}

	public int getNumItems() {
		return numItems;
	}

	public void setNumItems(int numItems) {
		this.numItems = numItems;
	}
	
	public User getPurchasedBy() {
		return purchasedBy;
	}

	public void setPurchasedBy(User purchasedBy) {
		this.purchasedBy = purchasedBy;
	}
	
	public List<Order> getOrders() {
		return orders;
	}

	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}

	public Timestamp getPurchasedAt() {
		return purchasedAt;
	}

	public void setPurchasedAt(Timestamp purchasedAt) {
		this.purchasedAt = purchasedAt;
	}

	public void addOrderToBill(Order order) {
		if(this.orders == null) {
			this.orders = new ArrayList<>();
		}
		this.orders.add(order);
	}

	@Override
	public String toString() {
		return "Bill [id=" + id + ", billAmount=" + billAmount + ", numItems=" + numItems + "]";
	}
}
