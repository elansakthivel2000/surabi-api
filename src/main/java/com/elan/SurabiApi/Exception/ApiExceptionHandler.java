package com.elan.SurabiApi.Exception;

import java.time.ZoneId;
import java.time.ZonedDateTime;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingRequestHeaderException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/*
 * 	Handler to handle ApiRequestionException
 * 
 */

@ControllerAdvice
public class ApiExceptionHandler {

	@ExceptionHandler(value = {ApiRequestException.class})
	public ResponseEntity<Object> handleApiException(ApiRequestException exception) {
		ApiException apiException = new ApiException(
				exception.getMessage(),
				HttpStatus.BAD_REQUEST,
				ZonedDateTime.now(ZoneId.of("Asia/Kolkata"))
			);
		return new ResponseEntity<>(apiException, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(value = {MissingRequestHeaderException.class})
	public ResponseEntity<Object> handleAuthorizationException(MissingRequestHeaderException exception) {
		ApiException apiException = new ApiException(
				"Please login to access this route. (Since we're not using session based or jwt based authentication"
				+ "... Login using login route and add userid as Authorization header to all the"
				+ " routes that needs login access.)",
				HttpStatus.BAD_REQUEST,
				ZonedDateTime.now(ZoneId.of("Asia/Kolkata"))
			);
		return new ResponseEntity<>(apiException, HttpStatus.BAD_REQUEST);
	}
}
	