package com.elan.SurabiApi.Logger;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;


/*
 * 	Logger implemented using AOP to log all the controller operations
 * 
 */
@Aspect
@Component
public class LoggingAdvice {
	
	Logger log = LoggerFactory.getLogger(LoggingAdvice.class);
	
	@Pointcut(value = "execution(* com.elan.SurabiApi.Controller.*.*(..)) || execution(* com.elan.SurabiApi.DAO.*.*(..))")
	public void myPointCut() {
		
	}
	
	@Around("myPointCut()")
	public Object applicationLogger(ProceedingJoinPoint joinPoint) throws Throwable {
		ObjectMapper mapper = new ObjectMapper();
		
		String methodName = joinPoint.getSignature().getName();
		String className = joinPoint.getTarget().getClass().toString();
		Object[] arguments = joinPoint.getArgs();
		
		log.info(">> "+className+" : Inside "+methodName+" method, arguments: "+mapper.writeValueAsString(arguments)+"\n");
		
		Object object = joinPoint.proceed();	
		log.info(">> "+className+" : Inside "+methodName+" method, Response: "+mapper.writeValueAsString(object)+"\n");
		
		return object;
	}

}
