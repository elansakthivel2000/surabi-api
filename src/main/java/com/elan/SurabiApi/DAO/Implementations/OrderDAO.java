package com.elan.SurabiApi.DAO.Implementations;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.exception.JDBCConnectionException;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import com.elan.SurabiApi.DAO.Interfaces.OrderDAOInterface;
import com.elan.SurabiApi.Entity.Bill;
import com.elan.SurabiApi.Entity.Order;
import com.elan.SurabiApi.Entity.Product;
import com.elan.SurabiApi.Entity.User;
import com.elan.SurabiApi.Exception.ApiRequestException;

@Repository
public class OrderDAO implements OrderDAOInterface {
	@Autowired
	private EntityManager entityManager;

	/**
	 * 	getAllOrders method is used to get all orders from database
	 * 	@param
	 *  @return list of orders if orders exists or null if orders table is empty.
	 */
	@Override
	public List<Order> getAllOrders() {
		try {
			// Get the session.
			Session session = entityManager.unwrap(Session.class);
			
			// Get all users from database
			Query<Order> query = session.createQuery("from Order", Order.class);
			List<Order> orders = query.getResultList();
			
			// return the users
			return orders;
			 
		// Exception Handling
		} catch(JDBCConnectionException e) {
			throw new ApiRequestException("Some problem with database connection.. Please try again later");
		} catch(Exception e) {
			throw new ApiRequestException("Something went wrong please try again later...");
		}
	}

	
	/**
	 * 	getOrder method is used to get particular order with given id.
	 * 	@param id - order id to find a particular order
	 *  @return Order object.
	 */
	@Override
	public Order getOrder(int id) {
		try {
			// Get the session
			Session session = entityManager.unwrap(Session.class);
			
			// Get the order using id
			Order order = session.get(Order.class, id);
			
			// if no order exists for that id return an exception.
			if(order == null) {
				throw new ApiRequestException("No order exists with given order id");
			}
			
			// othewise return order
			return order;
			
		// Exception handling
		} catch(ApiRequestException e) {
			throw e;
		} catch(JDBCConnectionException e) {
			throw new ApiRequestException("Some problem with database connection.. Please try again later");
		} catch(Exception e) {
			throw new ApiRequestException("Something went wrong please try again later...");
		}
		
	}

	
	/**
	 * 	addNewBill method is used to add a new order.
	 * 	@param order - order object holding all values
	 * 	@param userIdForAuth - user id of the user who is performing this operation (got from authorization header)
	 *  @return order object.
	 */
	@Override
	public Order addNewOrder(int userIdForAuth, Order order) {
		try {
			// Get the session
			Session session = entityManager.unwrap(Session.class);
			
			// get the current user who is performing this operation using id.
			User authUser = session.get(User.class, userIdForAuth);
			
			// if no user with that user id exists throw an exception.
			if(authUser == null) {
				throw new ApiRequestException("Auth User with given id doesn't exists");
			}
			
			// check if that user is logged in (because a user has to be logged in to access this route)
			if(authUser.isLoggedIn() == 0) {
				throw new ApiRequestException("Please login to access this route");
			}
			
			// check if the user is an admin
			if(!authUser.getRole().equals("ADMIN")) {
				throw new ApiRequestException("You're not allowed to perform this operation.");
			}
			
			// save and return the order
			session.save(order);
			return order;
			
		// Exception handling
		} catch(ApiRequestException e) {
			throw e;
		} catch(ConstraintViolationException e) {
			SQLException cause = (SQLException) e.getCause();
		    throw new ApiRequestException(cause.getMessage());
		} catch(JDBCConnectionException e) {
			throw new ApiRequestException("Some problem with database connection.. Please try again later");
		} catch(Exception e) {
			throw new ApiRequestException("Something went wrong please try again later...");
		}
	}

	/**
	 * 	updateOrder method is to update a Order with a particular id.
	 * 	@param userIdForAuth - user id of the user who is performing this operation (got from authorization header)
	 * 	@param id - Order id that we want to update.
	 * 	@param order - order object holding the values to update.
	 *  @return Order object with updated values.
	 */
	@Override
	@Transactional
	public Order updateOrder(int userIdForAuth, int id, Order order) {
		try {
			// Get the session
			Session session = entityManager.unwrap(Session.class);
			
			// get the current user who is performing this operation using id.
			User authUser = session.get(User.class, userIdForAuth);
			
			// if no user with that user id exists throw an exception.
			if(authUser == null) {
				throw new ApiRequestException("Auth User with given id doesn't exists");
			}
			
			// check if the user is actually logged in
			if(authUser.isLoggedIn() == 0) {
				throw new ApiRequestException("Please login to access this route");
			}
			
			// check if the user is an admin
			if(!authUser.getRole().equals("ADMIN")) {
				throw new ApiRequestException("You're not allowed to perform this operation.");
			}
			
			// get the order with the given id
			Order curOrder = session.get(Order.class, id);
			
			// if no order exists with the given id return an exception
			if(curOrder == null) {
				throw new ApiRequestException("No order exists with given order id");
			}
					
			// if product count is given in the body update it
			if(order.getProductCount() != 0) {
				curOrder.setProductCount(order.getProductCount());
			}
			
			// if new bill is given update the order to point to that bill
			if(order.getBill() != null) {
				Bill bill = session.get(Bill.class, order.getBill().getId());			
				if(bill == null) {
					throw new ApiRequestException("No Bill exists with given product id");
				}			
				curOrder.setBill(bill);
			}
			
			// if new product is given update the order to point to tha product
			if(order.getProduct() != null) {
				Product product = session.get(Product.class, order.getProduct().getId());
				if(product == null) {
					throw new ApiRequestException("No Product exists with given product id");
				}			
				curOrder.setProduct(product);
			}
			
			// save the return the updated order
			session.save(curOrder);
			session.flush();
			
			return curOrder;
			
		// Exception handling
		} catch(ApiRequestException e) {
			throw e;
		} catch(ConstraintViolationException e) {
			SQLException cause = (SQLException) e.getCause();
		    throw new ApiRequestException(cause.getMessage());
		} catch(JDBCConnectionException e) {
			throw new ApiRequestException("Some problem with database connection.. Please try again later");
		} catch(Exception e) {
			if(e.getClass() == PersistenceException.class) {
				throw new ApiRequestException("Updating this order failed, try another unique product - "
						+ "bill pair( cannot add same product to the same bill as a seperate entry).");
			}
			throw new ApiRequestException("Something went wrong please try again later...");
		}
	}

	/**
	 * 	deleteOrder method is to delete a order with a particular id.
	 * 	@param userIdForAuth - user id of the user who is performing this operation (got from authorization header)
	 * 	@param id - order id that we want to delete.
	 *  @return ResponseEntity<Object> json object with bill deleted message.
	 */
	@Override
	@Transactional
	public ResponseEntity<Object> deleteOrder(int userIdForAuth, int id) {
		try {
			// get the session
			Session session = entityManager.unwrap(Session.class);
			
			// get the current user who is performing this operation using id.
			User authUser = session.get(User.class, userIdForAuth);
			
			// if no user with that user id exists throw an exception.
			if(authUser == null) {
				throw new ApiRequestException("Auth User with given id doesn't exists");
			}
			
			// check if the user is actually logged in
			if(authUser.isLoggedIn() == 0) {
				throw new ApiRequestException("Please login to access this route");
			}
			
			// check if the user is an admin
			if(!authUser.getRole().equals("ADMIN")) {
				throw new ApiRequestException("You're not allowed to perform this operation.");
			}
			
			// get the order with the given id
			Order order = session.get(Order.class, id);
			
			// if no order exists with the given id return an exception
			if(order == null) {
				throw new ApiRequestException("order with given id does not exist");
			}
			
			// delete the order
			session.delete(order);
			session.flush();
			
			// construct the reponse
			String message = "Order with OrderId "+id+" successfully deleted.";
			HashMap<String, String> response = new HashMap<String, String>();
			response.put("message", message);
			
			// return response
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch(ApiRequestException e) {
			throw e;
		} catch(ConstraintViolationException e) {
			SQLException cause = (SQLException) e.getCause();
		    throw new ApiRequestException(cause.getMessage());
		} catch(JDBCConnectionException e) {
			throw new ApiRequestException("Some problem with database connection.. Please try again later");
		} catch(Exception e) {
			throw new ApiRequestException("Something went wrong please try again later...");
		}
	}
}
