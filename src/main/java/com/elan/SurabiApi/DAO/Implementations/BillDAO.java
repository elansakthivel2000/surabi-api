package com.elan.SurabiApi.DAO.Implementations;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.exception.JDBCConnectionException;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import com.elan.SurabiApi.DAO.Interfaces.BillDAOInterface;
import com.elan.SurabiApi.Entity.Bill;
import com.elan.SurabiApi.Entity.Order;
import com.elan.SurabiApi.Entity.Product;
import com.elan.SurabiApi.Entity.User;
import com.elan.SurabiApi.Exception.ApiRequestException;
import com.elan.SurabiApi.Model.Cart;
import com.elan.SurabiApi.Model.Report;

@Repository
public class BillDAO implements BillDAOInterface {

	@Autowired
	private EntityManager entityManager;
	
	/**
	 * 	getAllBills method is used to get all bills
	 *  @return list of Bills.
	 */
	@Override
	public List<Bill> getAllBills() {
		try {
			// Get the session.
			Session session = entityManager.unwrap(Session.class);
			
			// Get all bills from database
			Query<Bill> query = session.createQuery("from Bill", Bill.class);
			List<Bill> bills = query.getResultList();
			
			// return the bills
			return bills;
			
		// Exception handling
		} catch(ApiRequestException e) {
			throw e;
		} catch(JDBCConnectionException e) {
			throw new ApiRequestException("Some problem with database connection.. Please try again later");
		} catch(Exception e) {
			throw new ApiRequestException("Something went wrong please try again later...");
		}
	}
	
	/**
	 * 	getBill method is used to get particular bill with given id.
	 * 	@param id - bill id to find a particular bill
	 *  @return Bill object.
	 */
	@Override
	public Bill getBill(int id) {
		try {
			// Get the session
			Session session = entityManager.unwrap(Session.class);
			
			// Get the bill using id
			Bill bill = session.get(Bill.class, id);
			
			// if no bill exists for that id return an exception.
			if(bill == null) {
				throw new ApiRequestException("Bill with given id doesn't exists");
			}
			
			// otherwise return bill.
			return bill;
			
		// Exception handling
		} catch(ApiRequestException e) {
			throw e;
		} catch(JDBCConnectionException e) {
			throw new ApiRequestException("Some problem with database connection.. Please try again later");
		} catch(Exception e) {
			throw new ApiRequestException("Something went wrong please try again later...");
		}
	}

	
	/**
	 * 	addNewBill method is used to add a new Bill.
	 * 	@param cart - items of that bill represented by the {productId: count} structure
	 * 	@param userIdForAuth - user id of the user who is performing this operation (got from authorization header)
	 *  @return Bill object.
	 */
	@Override
	@Transactional
	public Bill addNewBill(Cart cart, int userIdForAuth) {
		try {
			// Get the session
			Session session = entityManager.unwrap(Session.class);
			
			// get the current user who is performing this operation using id.
			User authUser = session.get(User.class, userIdForAuth);
			
			// if no user with that user id exists throw an exception.
			if(authUser == null) {
				throw new ApiRequestException("user with given id doesn't exists");
			}
			
			// check if that user is logged in (because a user has to be logged in to access this route)
			if(authUser.isLoggedIn() == 0) {
				throw new ApiRequestException("Please login to access this route");
			}
			
			// create a new bill object
			Bill bill = new Bill();

			// generate bill amount and number of items in cart using the helper method.
			double[] cartDetails = getDetailsFromCart(session, cart);
			double amount = cartDetails[0];
			int numItems = (int) cartDetails[1];
			
			// set bill properties using the above values.
			bill.setBillAmount(amount);
			bill.setNumItems(numItems);
			bill.setPurchasedBy(authUser);
			
			// save the bill.
			session.save(bill);
			
			// loop through the cart and add products to products list.
			HashMap<Integer, Integer> productCountMap = cart.getCart();
			for(Map.Entry<Integer, Integer> entry: productCountMap.entrySet()) {	
				int productId = entry.getKey();
				
				// check if a product with product id in cart actually exists in our database.
				Product product = session.get(Product.class, productId);
				
				if(product == null) {
					throw new ApiRequestException("product with given id doesn't exists, cannot add a item to cart that doesn't exists");
				}
				
				// create a new order for every item in our cart
				Order newOrder = new Order();
				
				// establish the relation between product-order and bill-order
				newOrder.setProduct(product);
				newOrder.setBill(bill);
				newOrder.setProductCount(entry.getValue());
				
				// add that order to bill to establish bi-directional relation
				bill.addOrderToBill(newOrder);
				
				// save the order
				session.save(newOrder);
			}

			// save the bill
			session.save(bill);
			session.flush();
			
			// return bill
			return bill;
			
		// Exception handling
		} catch(ApiRequestException e) {
			throw e;
		} catch(ConstraintViolationException e) {
			SQLException cause = (SQLException) e.getCause();
		    throw new ApiRequestException(cause.getMessage());
		} catch(JDBCConnectionException e) {
			throw new ApiRequestException("Some problem with database connection.. Please try again later");
		} catch(Exception e) {
			throw new ApiRequestException("Something went wrong please try again later...");
		}
	}

	/**
	 * 	getDetailsFromCart method is a helper method to get total amount and number of items in a cart.
	 * 	@param session - session object to get products from database.
	 * 	@param cart - items of that bill represented by the {productId: count} structure
	 *  @return double[] having two values amount and num items.
	 */
	private double[] getDetailsFromCart(Session session, Cart cart) {
		double totalAmount = 0;
		double numItems = 0;
		
		// loop through the map and get the details
		HashMap<Integer, Integer> productCountMap = cart.getCart();
		for(Map.Entry<Integer, Integer> entry: productCountMap.entrySet()) {
			int productId = entry.getKey();
			Product product = session.get(Product.class, productId);
						
			totalAmount += product.getPrice();
			
			numItems += entry.getValue();
		}
		
		// return the details as double array
		return new double[]{totalAmount, numItems};
	}

	/**
	 * 	updateBill method is to update a bill with a particular id.
	 * 	@param userIdForAuth - user id of the user who is performing this operation (got from authorization header)
	 * 	@param billId - bill id that we want to update.
	 * 	@param bill - bill object holding the values to update.
	 *  @return Bill object with updated values.
	 */
	@Override
	@Transactional
	public Bill updateBill(int userIdForAuth, int billId, Bill bill) {
		try {
			// Get the session
			Session session = entityManager.unwrap(Session.class);
			
			// get the current user who is performing this operation using id.
			User authUser = session.get(User.class, userIdForAuth);
			
			// if no user with that user id exists throw an exception.
			if(authUser == null) {
				throw new ApiRequestException("Auth User with given id doesn't exists");
			}
			
			// check if the user is actually logged in
			if(authUser.isLoggedIn() == 0) {
				throw new ApiRequestException("Please login to access this route");
			}
			
			// check if the user is an admin
			if(!authUser.getRole().equals("ADMIN")) {
				throw new ApiRequestException("You're not allowed to perform this action");
			}
			
			
			// get the bill with the given id
			Bill curBill = session.get(Bill.class, billId);
			
			// if no bill exists with the given id return an exception
			if(curBill == null) {
				throw new ApiRequestException("Bill with given id doesn't exists");
			}
			
			// if bill amount is given in the body and it is greater than 0 update the bill with the new amount.
			if(bill.getBillAmount() != 0.0) {
				curBill.setBillAmount(bill.getBillAmount());
			}
			
			// if num items is given in the body and it is greater than 0 update the bill with the new num items.
			if(bill.getNumItems() != 0) {
				curBill.setNumItems(bill.getNumItems());			
			}
			
			// if new purchased by user is mentioned and the user actually exists update the bill to that user id.
			if(bill.getPurchasedBy() != null) {
				User user = session.get(User.class, bill.getPurchasedBy().getId());
				if(user == null) {
					throw new ApiRequestException("new user that you're trying to set with given id doesn't exists");
				}
				curBill.setPurchasedBy(user);
			}
		
			// save the updated bill.
			session.save(curBill);
			session.flush();
			
			return curBill;
			
		// Exception handling
		} catch(ApiRequestException e) {
			throw e;
		} catch(ConstraintViolationException e) {
			SQLException cause = (SQLException) e.getCause();
		    throw new ApiRequestException(cause.getMessage());
		} catch(JDBCConnectionException e) {
			throw new ApiRequestException("Some problem with database connection.. Please try again later");
		} catch(Exception e) {
			throw new ApiRequestException("Something went wrong please try again later...");
		}
	}

	
	/**
	 * 	deleteBill method is to delete a bill with a particular id.
	 * 	@param userIdForAuth - user id of the user who is performing this operation (got from authorization header)
	 * 	@param id - bill id that we want to delete.
	 *  @return ResponseEntity<Object> json object with bill deleted message.
	 */
	@Override
	@Transactional
	public ResponseEntity<Object> deleteBill(int userIdForAuth, int id) {
		try {
			// get the session
			Session session = entityManager.unwrap(Session.class);

			// get the current user who is performing this operation using id.
			User authUser = session.get(User.class, userIdForAuth);
			
			// if no user with that user id exists throw an exception.
			if(authUser == null) {
				throw new ApiRequestException("Auth User with given id doesn't exists");
			}
			
			// check if the user is actually logged in
			if(authUser.isLoggedIn() == 0) {
				throw new ApiRequestException("Please login to access this route");
			}
			
			// get the bill with the given id
			Bill bill = session.get(Bill.class, id);			
			
			// if no bill exists with the given id return an exception
			if(bill == null) {
				throw new ApiRequestException("Bill with given id doesn't exists");
			}
				
			// check if the user is an admin
			if(!authUser.getRole().equals("ADMIN")) {
				throw new ApiRequestException("You're not allowed to perform this action");
			}
			
			// delete the bill
			session.delete(bill);
			session.flush();
			
			// construct the response
			String message = "Bill with billId "+id+" successfully deleted.";
			HashMap<String, String> response = new HashMap<String, String>();
			response.put("message", message);
			
			// return the response
			return new ResponseEntity<>(response, HttpStatus.OK);
			
		// Exception handling
		} catch(ApiRequestException e) {
			throw e;
		} catch(JDBCConnectionException e) {
			throw new ApiRequestException("Some problem with database connection.. Please try again later");
		} catch(Exception e) {
			throw new ApiRequestException("Something went wrong please try again later...");
		}
	}

	
	/**
	 * 	This method is to get all the bills generated today.
	 * 	@param userIdForAuth - user id of the user who is performing this operation (got from authorization header)
	 * 	@return list of bills if exists otherwise a message saying no purchases today.
	 */
	@Override
	public List<Bill> getAllBillsForToday(int userIdForAuth) {
		try {
			// get the session
			Session session = entityManager.unwrap(Session.class);
			
			// get the current user who is performing this operation using id.
			User authUser = session.get(User.class, userIdForAuth);
			
			// if no user with that user id exists throw an exception.
			if(authUser == null) {
				throw new ApiRequestException("Auth User with given id doesn't exists");
			}
			
			// check if the user is actually logged in
			if(authUser.isLoggedIn() == 0) {
				throw new ApiRequestException("Please login to access this route");
			}
			
			// check if the user is an admin
			if(!authUser.getRole().equals("ADMIN")) {
				throw new ApiRequestException("You're not allowed to perform this action");
			}
			
			// get the list of bills using HQL query
			Query<Bill> query = session.createQuery("from Bill where DATE(purchasedAt) = CURDATE()", Bill.class);
			List<Bill> billsForToday = query.getResultList();
			
			// if no bills return no purchases today message as json
			if(billsForToday == null || billsForToday.size() == 0) {
				throw new ApiRequestException("No purchases today.");
			}
			
			// otherwise return list of bills
			return billsForToday;
			
		// Exception
		} catch(ApiRequestException e) {
			throw e;
		} catch(JDBCConnectionException e) {
			throw new ApiRequestException("Some problem with database connection.. Please try again later");
		} catch(Exception e) {
			throw new ApiRequestException("Something went wrong please try again later...");
		}
	}

	
	/**
	 * 	This method is to get this month sales report.
	 * 	@param userIdForAuth - user id of the user who is performing this operation (got from authorization header)
	 * 	@return Report object if any purchases this month otherwise a message saying no purchases this month.
	 */
	@Override
	public Report getTotalSaleForMonth(int userIdForAuth) {
		try {
			// get the session
			Session session = entityManager.unwrap(Session.class);
			
			// get the current user who is performing this operation using id.
			User authUser = session.get(User.class, userIdForAuth);
			
			// if no user with that user id exists throw an exception.
			if(authUser == null) {
				throw new ApiRequestException("Auth User with given id doesn't exists");
			}
			
			// check if the user is actually logged in
			if(authUser.isLoggedIn() == 0) {
				throw new ApiRequestException("Please login to access this route");
			}
			
			// check if the user is an admin
			if(!authUser.getRole().equals("ADMIN")) {
				throw new ApiRequestException("You're not allowed to perform this action");
			}
			
			// get the list of bills using HQL query
			Query<Bill> query = session.createQuery("from Bill where MONTHNAME(purchasedAt) = MONTHNAME(CURDATE())", Bill.class);
			List<Bill> bills = query.getResultList();
			
			// if no bills return no purchases today message as json
			if(bills == null || bills.size() == 0) {
				throw new ApiRequestException("No purchases this month..");
			}
			
			// generate report from list of bills
			Report report = createReportFromListOfBills(bills);
			
			// return report
			return report;
			
		// Exception
		} catch(ApiRequestException e) {
			throw e;
		} catch(JDBCConnectionException e) {
			throw new ApiRequestException("Some problem with database connection.. Please try again later");
		} catch(Exception e) {
			System.out.println(e);
			throw new ApiRequestException("Something went wrong please try again later...");
		}
	}

	/**
	 * 	This method is a helper method to generate report from bills.
	 * 	@param bills - list of bills
	 * 	@return Report object generated from bills.
	 */
	private Report createReportFromListOfBills(List<Bill> bills) {
		Timestamp timestamp = bills.get(0).getPurchasedAt();
		LocalDateTime dateTime = timestamp.toLocalDateTime();
		
		// get year and date from timestamp
		int year = dateTime.getYear();
		int month = dateTime.getMonthValue();
		
		// loop through the bills and get total amount
		double totalSale = 0;
		for(Bill bill: bills) {
			totalSale += bill.getBillAmount();
		}
		
		// return the report
		return new Report(year, month,bills.size(), totalSale);
	}
}