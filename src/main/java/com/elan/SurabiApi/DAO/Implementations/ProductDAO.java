package com.elan.SurabiApi.DAO.Implementations;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.exception.JDBCConnectionException;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import com.elan.SurabiApi.DAO.Interfaces.ProductDAOInterface;
import com.elan.SurabiApi.Entity.Product;
import com.elan.SurabiApi.Entity.User;
import com.elan.SurabiApi.Exception.ApiRequestException;

/*
 * 	ProductDAO CRUD operations are similar to OrderDAO CRUD operations
 * 	refer:- OrderDAO for comments
 * 
 */

@Repository
public class ProductDAO implements ProductDAOInterface {
	
	@Autowired
	private EntityManager entityManager;

	@Override
	public List<Product> getAllProducts() {
		try {
			Session session = entityManager.unwrap(Session.class);
			Query<Product> query = session.createQuery("from Product", Product.class);
			List<Product> products = query.getResultList();
			return products;
		} catch(JDBCConnectionException e) {
			throw new ApiRequestException("Some problem with database connection.. Please try again later");
		} catch(Exception e) {
			throw new ApiRequestException("Something went wrong please try again later...");
		}
	}

	@Override
	public Product getProduct(int id) {
		try {
			Session session = entityManager.unwrap(Session.class);
			Product product = session.get(Product.class, id);
			
			if(product == null) {
				throw new ApiRequestException("No product exists with given product id");
			}
			
			return product;
		} catch(ApiRequestException e) {
			throw e;
		} catch(JDBCConnectionException e) {
			throw new ApiRequestException("Some problem with database connection.. Please try again later");
		} catch(Exception e) {
			throw new ApiRequestException("Something went wrong please try again later...");
		}
		
	}

	@Override
	public Product addNewProduct(int userIdForAuth, Product product) {
		try {
			Session session = entityManager.unwrap(Session.class);
			
			User authUser = session.get(User.class, userIdForAuth);
			
			if(authUser == null) {
				throw new ApiRequestException("Auth User with given id doesn't exists");
			}
			
			if(authUser.isLoggedIn() == 0) {
				throw new ApiRequestException("Please login to access this route");
			}
			
			if(!authUser.getRole().equals("ADMIN")) {
				throw new ApiRequestException("You're not allowed to perform this operation.");
			}
			
			session.save(product);
			return product;
		} catch(ApiRequestException e) {
			throw e;
		} catch(ConstraintViolationException e) {
			SQLException cause = (SQLException) e.getCause();
		    throw new ApiRequestException(cause.getMessage());
		} catch(JDBCConnectionException e) {
			throw new ApiRequestException("Some problem with database connection.. Please try again later");
		} catch(Exception e) {
			throw new ApiRequestException("Something went wrong please try again later...");
		}
	}

	@Override
	@Transactional
	public Product updateProduct(int userIdForAuth, int id, Product product) {
		try {
			Session session = entityManager.unwrap(Session.class);
			
			User authUser = session.get(User.class, userIdForAuth);
			
			if(authUser == null) {
				throw new ApiRequestException("Auth User with given id doesn't exists");
			}
			
			if(authUser.isLoggedIn() == 0) {
				throw new ApiRequestException("Please login to access this route");
			}
			
			if(!authUser.getRole().equals("ADMIN")) {
				throw new ApiRequestException("You're not allowed to perform this operation.");
			}
			
			Product curProduct = session.get(Product.class, id);
			
			if(curProduct == null) {
				throw new ApiRequestException("No product exists with given product id");
			}
					
			if(product.getPrice() != 0.0) {
				curProduct.setPrice(product.getPrice());
			}
			
			if(product.getProductName() != null) {
				curProduct.setProductName(product.getProductName());			
			}
			
			session.save(curProduct);
			session.flush();
			
			return curProduct;
		} catch(ApiRequestException e) {
			throw e;
		} catch(ConstraintViolationException e) {
			SQLException cause = (SQLException) e.getCause();
		    throw new ApiRequestException(cause.getMessage());
		} catch(JDBCConnectionException e) {
			throw new ApiRequestException("Some problem with database connection.. Please try again later");
		} catch(Exception e) {
			if(e.getClass() == PersistenceException.class) {
				throw new ApiRequestException("Updating to this product name failed, try another unique product.");
			}
			throw new ApiRequestException("Something went wrong please try again later...");
		}
	}

	@Override
	@Transactional
	public ResponseEntity<Object> deleteProduct(int userIdForAuth, int id) {
		try {
			Session session = entityManager.unwrap(Session.class);
			
			User authUser = session.get(User.class, userIdForAuth);
			
			if(authUser == null) {
				throw new ApiRequestException("Auth User with given id doesn't exists");
			}
			
			if(authUser.isLoggedIn() == 0) {
				throw new ApiRequestException("Please login to access this route");
			}
			
			if(!authUser.getRole().equals("ADMIN")) {
				throw new ApiRequestException("You're not allowed to perform this operation.");
			}
			
			Product product = session.get(Product.class, id);
			
			if(product == null) {
				throw new ApiRequestException("Product with given id does not exist");
			}
			
			session.delete(product);
			session.flush();
			
			String message = "Product with productId "+id+" successfully deleted.";
			HashMap<String, String> response = new HashMap<String, String>();
			response.put("message", message);
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch(ApiRequestException e) {
			throw e;
		} catch(ConstraintViolationException e) {
			SQLException cause = (SQLException) e.getCause();
		    throw new ApiRequestException(cause.getMessage());
		} catch(JDBCConnectionException e) {
			throw new ApiRequestException("Some problem with database connection.. Please try again later");
		} catch(Exception e) {
			throw new ApiRequestException("Something went wrong please try again later...");
		}
	}

}
