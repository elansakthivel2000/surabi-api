package com.elan.SurabiApi.DAO.Implementations;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.exception.JDBCConnectionException;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import com.elan.SurabiApi.DAO.Interfaces.UserDAOInterface;
import com.elan.SurabiApi.Entity.Bill;
import com.elan.SurabiApi.Entity.User;
import com.elan.SurabiApi.Exception.ApiRequestException;

@Repository
public class UserDAO implements UserDAOInterface {
	@Autowired
	private EntityManager entityManager;
	
	/**
	 * 	login method is add a new Bill.
	 * 	@param user - user object holding the user values.
	 *  @return User object.
	 */
	@Override
	@Transactional
	public User login(User user) {
		try {
			// Get the session
			Session session = entityManager.unwrap(Session.class);
			
			// write hql query to get the user with the given username and password.
			Query<User> query = session.createQuery("from User where username= :username and password= :password", User.class);
			query.setParameter("username", user.getUsername());
			query.setParameter("password", user.getPassword());
			
			// if no users exists with that username and password return an exception
			List<User> users = query.getResultList();
			if(users.size() == 0) {
				throw new ApiRequestException("Incorrect username or password");
			} 
			
			/*
			 * otherwise set the user as loggedin in database 
			 * (since we're not implementing jwt or session based auth, i'm storing the actual logged in or not value in the
			 *  database for the proof that a user is logged in)
			 */
			User curUser = users.get(0);
			curUser.setLoggedIn(1);
			session.save(curUser);
			session.flush();
			
			// return the user
			return users.get(0);

		// Exception handling
		} catch(ApiRequestException e) {
			throw e;
		} catch(ConstraintViolationException e) {
			SQLException cause = (SQLException) e.getCause();
		    throw new ApiRequestException(cause.getMessage());
		} catch(JDBCConnectionException e) {
			throw new ApiRequestException("Some problem with database connection.. Please try again later");
		} catch(Exception e) {
			throw new ApiRequestException("Something went wrong please try again later...");
		}
	}

	/**
	 * 	Register method is used to sign up new users
	 * 	@param user - user object holding the values of user
	 *  @return user if successful otherwise null
	 */
	@Override
	public User register(User user) {
		try {
			// check if the username and password is blank
			if(user.getUsername().isBlank() || user.getPassword().isBlank()) {
				throw new ApiRequestException("Username and password cannot be empty.");
			}
			
			// get the session
			Session session = entityManager.unwrap(Session.class);
			
			// save the user
			session.save(user);
			
			// return the user
			return user;
			
		// Exception handling
		} catch(ApiRequestException e) {
			throw e;
		} catch(ConstraintViolationException e) {
			SQLException cause = (SQLException) e.getCause();
		    throw new ApiRequestException(cause.getMessage());
		} catch(JDBCConnectionException e) {
			throw new ApiRequestException("Some problem with database connection.. Please try again later");
		} catch(Exception e) {
			throw new ApiRequestException("Something went wrong please try again later...");
		}	
	}

	/**
	 * 	getAllUsers method is used to get all users from database
	 * 	@param
	 *  @return list of users if users exists or null if users table is empty.
	 */
	@Override
	public List<User> getAllUsers() {
		try {
			// Get the session.
			Session session = entityManager.unwrap(Session.class);
			
			// Get all users from database
			Query<User> query = session.createQuery("from User", User.class);
			List<User> users = query.getResultList();
			
			// return the users
			return users;
		
		// Exception handling
		} catch(ApiRequestException e) {
			throw e;
		} catch(JDBCConnectionException e) {
			throw new ApiRequestException("Some problem with database connection.. Please try again later");
		} catch(Exception e) {
			throw new ApiRequestException("Something went wrong please try again later...");
		}
	}

	/**
	 * 	getUser method is used to get particular user with given id.
	 * 	@param id - user id to find a particular user
	 *  @return User object.
	 */
	@Override
	public User getUser(int id) {
		try {
			// Get the session
			Session session = entityManager.unwrap(Session.class);
			
			// Get the user using id
			User user = session.get(User.class, id);
			
			// if no user exists for that id return an exception.
			if(user == null) {
				throw new ApiRequestException("User with given id doesn't exists");
			}
			
			// othewise return user
			return user;
			
		// Exception handling
		} catch(ApiRequestException e) {
			throw e;
		} catch(JDBCConnectionException e) {
			throw new ApiRequestException("Some problem with database connection.. Please try again later");
		} catch(Exception e) {
			throw new ApiRequestException("Something went wrong please try again later...");
		}
	}

	
	/**
	 * 	updateUser method is to update a user with a particular id.
	 * 	@param userIdForAuth - user id of the user who is performing this operation (got from authorization header)
	 * 	@param id - user id that we want to update.
	 * 	@param user - user object holding the values to update.
	 *  @return User object with updated values.
	 */
	@Override
	@Transactional
	public User updateUser(int userIdForAuth, int id, User user) {
		try {
			// Get the session
			Session session = entityManager.unwrap(Session.class);
			
			// get the current user who is performing this operation using id.
			User authUser = session.get(User.class, userIdForAuth);
			
			// if no user with that user id exists throw an exception.
			if(authUser == null) {
				throw new ApiRequestException("Auth User with given id doesn't exists");
			}
			
			// check if the user is actually logged in
			if(authUser.isLoggedIn() == 0) {
				throw new ApiRequestException("Please login to access this route");
			}
			
			// get the user with the given id
			User currentUser = session.get(User.class, id);
			
			// if no user exists with the given id return an exception
			if(currentUser == null) {
				throw new ApiRequestException("User with given id doesn't exists");
			}		
			
			// check if the user is an admin or the corresponding user
			if(!authUser.getRole().equals("ADMIN") && currentUser.getId() != userIdForAuth) {
				throw new ApiRequestException("You're not allowed to perform this action");
			}
					
			// if username is given in the body update it
			if(user.getUsername() != null) {
				currentUser.setUsername(user.getUsername());
			}
			
			// if password is given in the body update it
			if(user.getPassword() != null) {
				currentUser.setPassword(user.getPassword());
			}
			
			// save the user
			session.save(currentUser);
			session.flush();
					
			// return the user
			return currentUser;
			
		// Exception handling
		} catch(ApiRequestException e) {
			throw e;
		} catch(ConstraintViolationException e) {
			SQLException cause = (SQLException) e.getCause();
		    throw new ApiRequestException(cause.getMessage());
		} catch(JDBCConnectionException e) {
			throw new ApiRequestException("Some problem with database connection.. Please try again later");
		} catch(Exception e) {
			if(e.getClass() == PersistenceException.class) {
				throw new ApiRequestException("Updating to this username failed, try another unique username.");
			}
			throw new ApiRequestException("Something went wrong please try again later...");
		}
	}

	/**
	 * 	deleteUser method is to delete a user with a particular id.
	 * 	@param userIdForAuth - user id of the user who is performing this operation (got from authorization header)
	 * 	@param id - user id that we want to delete.
	 *  @return ResponseEntity<Object> json object with user deleted message.
	 */
	@Override
	@Transactional
	public ResponseEntity<Object> deleteUser(int userIdForAuth, int id) {
		try {
			// get the session
			Session session = entityManager.unwrap(Session.class);
			
			// get the current user who is performing this operation using id.
			User authUser = session.get(User.class, userIdForAuth);
			
			// if no user with that user id exists throw an exception.
			if(authUser == null) {
				throw new ApiRequestException("Auth User with given id doesn't exists");
			}	
			
			// check if the user is actually logged in
			if(authUser.isLoggedIn() == 0) {
				throw new ApiRequestException("Please login to access this route");
			}
			
			// get the user with the given id
			User userToDelete = session.get(User.class, id);
			
			// if no user exists with the given id return an exception
			if(userToDelete == null) {
				throw new ApiRequestException("User with given id doesn't exists");
			}
			
			// check if the user is an admin or the corresponding user
			if(!authUser.getRole().equals("ADMIN") && userToDelete.getId() != userIdForAuth) {
				throw new ApiRequestException("You're not allowed to perform this action");
			}
			
			// get all the purchases made by him/her
			List<Bill> purchases = userToDelete.getPurchases();
			
			// make purchasedBy field in all the purchased he/she made to null
			if(purchases != null) {
				for(Bill bill:purchases) {
					bill.setPurchasedBy(null);
				}
			}
			
			// delete the user
			session.delete(userToDelete);
			session.flush();
			
			// construct the response
			String message = "User "+userToDelete.getUsername()+" with userid "+id+" successfully deleted.";
			HashMap<String, String> response = new HashMap<String, String>();
			response.put("message", message);
			
			// return the response
			return new ResponseEntity<>(response, HttpStatus.OK);
			
		// Exception handling
		} catch(ApiRequestException e) {
			throw e;
		} catch(ConstraintViolationException e) {
			SQLException cause = (SQLException) e.getCause();
		    throw new ApiRequestException(cause.getMessage());
		} catch(JDBCConnectionException e) {
			throw new ApiRequestException("Some problem with database connection.. Please try again later");
		} catch(Exception e) {
			throw new ApiRequestException("Something went wrong please try again later...");
		}
	}

	
	/**
	 * 	This method is to logout the user who is performing this operation.
	 * 	@param userIdForAuth - user id of the user who is performing this operation (got from authorization header)
	 * 	@return list of bills if exists otherwise a message saying no purchases today.
	 */
	@Override
	@Transactional
	public ResponseEntity<Object> logout(int userIdForAuth) {
		try {			
			// get the session
			Session session = entityManager.unwrap(Session.class);
			
			// get the current user who is performing this operation using id.
			User authUser = session.get(User.class, userIdForAuth);
			
			// if no user with that user id exists throw an exception.
			if(authUser == null) {
				throw new ApiRequestException("User with given id doesn't exists");
			}
			
			// check if the user is actually logged in (a user has to be logged in to logout)
			if(authUser.isLoggedIn() == 0) {
				throw new ApiRequestException("You have to be logged in first to perform logout operation");
			}
			
			// set the logged field in entity to 0 meaning he is now logged out
			authUser.setLoggedIn(0);
			
			// save the user
			session.save(authUser);
			session.flush();
			
			// construct the response
			String message = "User "+authUser.getUsername()+" successfully logged out.";
			HashMap<String, String> response = new HashMap<String, String>();
			response.put("message", message);
			
			// return the response
			return new ResponseEntity<>(response, HttpStatus.OK);
		} catch(ApiRequestException e) {
			throw e;
		} catch(JDBCConnectionException e) {
			throw new ApiRequestException("Some problem with database connection.. Please try again later");
		} catch(Exception e) {
			throw new ApiRequestException("Something went wrong please try again later...");
		}
	}
}
