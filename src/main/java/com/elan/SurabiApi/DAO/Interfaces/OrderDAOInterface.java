package com.elan.SurabiApi.DAO.Interfaces;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.elan.SurabiApi.Entity.Order;

public interface OrderDAOInterface {
	List<Order> getAllOrders();
	Order getOrder(int id);
	Order addNewOrder(int userIdForAuth, Order order);
	Order updateOrder(int userIdForAuth, int id, Order order);
	ResponseEntity<Object> deleteOrder(int userIdForAuth, int id);
}
