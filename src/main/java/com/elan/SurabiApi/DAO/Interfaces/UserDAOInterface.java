package com.elan.SurabiApi.DAO.Interfaces;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.elan.SurabiApi.Entity.User;

/*
 * 	User DAO interface
 */

public interface UserDAOInterface {
	List<User> getAllUsers();
	User getUser(int id);
	User updateUser(int userIdForAuth, int id, User user);
	ResponseEntity<Object> deleteUser(int userIdForAuth, int id);
	User login(User user);
	User register(User user);
	ResponseEntity<Object> logout(int id);
}
