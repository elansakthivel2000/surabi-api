package com.elan.SurabiApi.DAO.Interfaces;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.elan.SurabiApi.Entity.Bill;
import com.elan.SurabiApi.Model.Cart;
import com.elan.SurabiApi.Model.Report;

public interface BillDAOInterface {
	List<Bill> getAllBills();
	Bill getBill(int id);
	Bill addNewBill(Cart cart, int userid);
	Bill updateBill(int userIdForAuth, int id, Bill bill);
	ResponseEntity<Object> deleteBill(int userIdForAuth, int id);
	List<Bill> getAllBillsForToday(int userIdForAuth);
	Report getTotalSaleForMonth(int userIdForAuth);
}
