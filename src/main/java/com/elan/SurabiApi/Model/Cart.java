package com.elan.SurabiApi.Model;

import java.util.HashMap;

import org.springframework.stereotype.Component;

@Component
public class Cart {
	private HashMap<Integer, Integer> cart;
	
	public Cart() {
		
	}

	public Cart(HashMap<Integer, Integer> cart) {
		this.cart = cart;
	}

	public HashMap<Integer, Integer> getCart() {
		return cart;
	}

	public void setCart(HashMap<Integer, Integer> cart) {
		this.cart = cart;
	}

	@Override
	public String toString() {
		return "Cart [cart=" + cart + "]";
	}
}
