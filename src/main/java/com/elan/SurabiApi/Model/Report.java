package com.elan.SurabiApi.Model;

import org.springframework.stereotype.Component;

@Component
public class Report {
	private int year;
	private int month;
	private double totalSale;
	private int numSales;
	
	public Report() {
		
	}
	
	public Report(int year, int month,int numSales, double totalSale) {
		this.year = year;
		this.month = month;
		this.numSales = numSales;
		this.totalSale = totalSale;
	}

	public int getYear() {
		return year;
	}

	public int getMonth() {
		return month;
	}

	public double getTotalSale() {
		return totalSale;
	}
	
	public int getNumSales() {
		return numSales;
	}

	public void setNumSales(int numSales) {
		this.numSales = numSales;
	}

	@Override
	public String toString() {
		return "Report [year=" + year + ", month=" + month + ", totalSale=" + totalSale + "]";
	}

}
