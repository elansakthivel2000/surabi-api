package com.elan.SurabiApi.Service.Implementations;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.elan.SurabiApi.DAO.Implementations.BillDAO;
import com.elan.SurabiApi.Entity.Bill;
import com.elan.SurabiApi.Model.Cart;
import com.elan.SurabiApi.Model.Report;
import com.elan.SurabiApi.Service.Interfaces.BillServiceInterface;

@Service
public class BillService implements BillServiceInterface {

	@Autowired
	private BillDAO billDao;
	
	@Override
	public List<Bill> getAllBills() {
		return billDao.getAllBills();
	}

	@Override
	public Bill getBill(int id) {
		return billDao.getBill(id);
	}

	@Override
	public Bill addNewBill(Cart cart, int userId) {
		return billDao.addNewBill(cart, userId);
	}

	@Override
	public Bill updateBill(int userIdForAuth, int id, Bill bill) {
		return billDao.updateBill(userIdForAuth, id, bill);
	}

	@Override
	public ResponseEntity<Object> deleteBill(int userIdForAuth, int id) {
		return billDao.deleteBill(userIdForAuth, id);
	}

	@Override
	public List<Bill> getAllBillsForToday(int userIdForAuth) {
		return billDao.getAllBillsForToday(userIdForAuth);
	}

	@Override
	public Report getTotalSalesForMonth(int userIdForAuth) {
		return billDao.getTotalSaleForMonth(userIdForAuth);
	}

}
