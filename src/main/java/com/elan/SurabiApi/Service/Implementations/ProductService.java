package com.elan.SurabiApi.Service.Implementations;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.elan.SurabiApi.DAO.Interfaces.ProductDAOInterface;
import com.elan.SurabiApi.Entity.Product;
import com.elan.SurabiApi.Service.Interfaces.ProductServiceInterface;

@Service
public class ProductService implements ProductServiceInterface {

	@Autowired
	private ProductDAOInterface productDAO;
	
	@Override
	public List<Product> getAllProducts() {
		return productDAO.getAllProducts();
	}

	@Override
	public Product getProduct(int id) {
		return productDAO.getProduct(id);
	}

	@Override
	public Product addNewProduct(int userIdForAuth, Product product) {
		return productDAO.addNewProduct(userIdForAuth, product);
	}

	@Override
	public Product updateProduct(int userIdForAuth, int id, Product product) {
		return productDAO.updateProduct(userIdForAuth, id, product);
	}

	@Override
	public ResponseEntity<Object> deleteProduct(int userIdForAuth, int id) {
		return productDAO.deleteProduct(userIdForAuth, id);
	}

}
