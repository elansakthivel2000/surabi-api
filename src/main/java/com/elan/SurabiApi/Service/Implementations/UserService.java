package com.elan.SurabiApi.Service.Implementations;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.elan.SurabiApi.DAO.Interfaces.UserDAOInterface;
import com.elan.SurabiApi.Entity.User;
import com.elan.SurabiApi.Service.Interfaces.UserServiceInterface;

/*
 * 	UserService acts as an interface for user controller to access userDAO methods.
 * 
 */
@Service
public class UserService implements UserServiceInterface {
	@Autowired
	private UserDAOInterface userDAO;
	
	@Override
	public User login(User user) {
		return userDAO.login(user);
	}

	@Override
	public User register(User user) {
		return userDAO.register(user);
	}

	@Override
	public List<User> getAllUsers() {
		return userDAO.getAllUsers();
	}

	@Override
	public User getUser(int id) {
		return userDAO.getUser(id);
	}

	@Override
	public User updateUser(int userIdForAuth, int id, User user) {
		return userDAO.updateUser(userIdForAuth, id, user);
	}

	@Override
	public ResponseEntity<Object> deleteUser(int userIdForAuth, int id) {
		return userDAO.deleteUser(userIdForAuth, id);
	}

	@Override
	public ResponseEntity<Object> logout(int id) {
		return userDAO.logout(id);
	}
}
