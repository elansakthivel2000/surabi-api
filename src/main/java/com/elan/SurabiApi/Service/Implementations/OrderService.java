package com.elan.SurabiApi.Service.Implementations;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.elan.SurabiApi.DAO.Interfaces.OrderDAOInterface;
import com.elan.SurabiApi.Entity.Order;
import com.elan.SurabiApi.Service.Interfaces.OrderServiceInterface;

@Service
public class OrderService implements OrderServiceInterface {

	@Autowired
	private OrderDAOInterface orderDao;
	
	@Override
	public List<Order> getAllOrders() {
		return orderDao.getAllOrders();
	}

	@Override
	public Order getOrder(int id) {
		return orderDao.getOrder(id);
	}

	@Override
	public Order addNewOrder(int userIdForAuth, Order order) {
		return orderDao.addNewOrder(userIdForAuth, order);
	}

	@Override
	public Order updateOrder(int userIdForAuth, int id, Order order) {
		return orderDao.updateOrder(userIdForAuth, id, order);
	}

	@Override
	public ResponseEntity<Object> deleteOrder(int userIdForAuth, int id) {
		return orderDao.deleteOrder(userIdForAuth, id);
	}

}
