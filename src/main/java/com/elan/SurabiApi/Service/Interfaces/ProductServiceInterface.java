package com.elan.SurabiApi.Service.Interfaces;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.elan.SurabiApi.Entity.Product;

public interface ProductServiceInterface {
	List<Product> getAllProducts();
	Product getProduct(int id);
	Product addNewProduct(int userIdForAuth, Product product);
	Product updateProduct(int userIdForAuth, int id, Product product);
	ResponseEntity<Object> deleteProduct(int userIdForAuth, int id);
}
