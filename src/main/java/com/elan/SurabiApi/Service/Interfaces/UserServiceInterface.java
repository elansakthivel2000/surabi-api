package com.elan.SurabiApi.Service.Interfaces;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.elan.SurabiApi.Entity.User;

public interface UserServiceInterface {
	User login(User user);
	User register(User user);
	List<User> getAllUsers();
	User getUser(int id);
	User updateUser(int userIdForAuth, int id, User user);
	ResponseEntity<Object> deleteUser(int userIdForAuth, int id);
	ResponseEntity<Object> logout(int id);
}
