package com.elan.SurabiApi.Service.Interfaces;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.elan.SurabiApi.Entity.Bill;
import com.elan.SurabiApi.Model.Cart;
import com.elan.SurabiApi.Model.Report;

public interface BillServiceInterface {
	List<Bill> getAllBills();
	Bill getBill(int id);
	Bill addNewBill(Cart cart, int userId);
	Bill updateBill(int userIdForAuth, int id, Bill bill);
	ResponseEntity<Object> deleteBill(int userIdForAuth, int id);
	List<Bill> getAllBillsForToday(int userIdForAuth);
	Report getTotalSalesForMonth(int userIdForAuth);
}
