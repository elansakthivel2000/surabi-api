package com.elan.SurabiApi.Service.Interfaces;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.elan.SurabiApi.Entity.Order;

public interface OrderServiceInterface {
	List<Order> getAllOrders();
	Order getOrder(int id);
	Order addNewOrder(int userIdForAuth, Order order);
	Order updateOrder(int userIdForAuth, int id, Order order);
	ResponseEntity<Object> deleteOrder(int userIdForAuth, int id);
}
