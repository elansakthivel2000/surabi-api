package com.elan.SurabiApi.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.elan.SurabiApi.Entity.Order;
import com.elan.SurabiApi.Exception.ApiRequestException;
import com.elan.SurabiApi.Service.Interfaces.OrderServiceInterface;

@RestController
@RequestMapping("/api/orders")
@CrossOrigin(origins = {"http://localhost:3000", "http://localhost:4200"})
public class OrderController {

	@Autowired
	private OrderServiceInterface orderService;
	
	@GetMapping
	public List<Order> Order() {
		return orderService.getAllOrders();
	}
	
	@PostMapping
	public Order addOrder(@RequestHeader("Authorization") int userIdForAuth, @RequestBody Order order) {
		if(
			order.getProductCount() == 0 ||
			order.getBill() == null ||
			order.getBill().getId() == 0 ||
			order.getProduct() == null ||
			order.getProduct().getId() == 0
		) {
			throw new ApiRequestException("Please provide product count, bill id and product id to add a new order.");
		}

		return orderService.addNewOrder(userIdForAuth, order);
	}
	
	@GetMapping("/{orderId}")
	public Order getProduct(@PathVariable int orderId) {
		return orderService.getOrder(orderId);
	}
	
	@PatchMapping("/{orderId}")
	public Order updateProduct(@RequestHeader("Authorization") int userIdForAuth, @PathVariable int orderId, @RequestBody Order order) {
		if(
			order.getProductCount() == 0 &&
			order.getBill() == null &&
			order.getBill().getId() == 0 &&
			order.getProduct() == null &&
			order.getProduct().getId() == 0
		) {
			throw new ApiRequestException("Please provide product count or bill id or product id to update a order.");
		}
		return orderService.updateOrder(userIdForAuth, orderId, order);
	}
	
	@DeleteMapping("/{orderId}")
	public ResponseEntity<Object> deleteProduct(@RequestHeader("Authorization") int userIdForAuth, @PathVariable int orderId) {
		return orderService.deleteOrder(userIdForAuth, orderId);
	}
}
