package com.elan.SurabiApi.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.elan.SurabiApi.Entity.Product;
import com.elan.SurabiApi.Exception.ApiRequestException;
import com.elan.SurabiApi.Service.Interfaces.ProductServiceInterface;

/*
 *  Product controller has all the routes related to product operation.
 *  	- For request parameters and response body details view API documentation.
 * 
 */

@RestController
@RequestMapping("/api/products")
@CrossOrigin(origins = {"http://localhost:3000", "http://localhost:4200"})
public class ProductController {

	@Autowired
	private ProductServiceInterface productService;
	
	@GetMapping
	public List<Product> getAllProducts() {
		return productService.getAllProducts();
	}
	
	@PostMapping
	public Product addProduct(@RequestHeader("Authorization") int userIdForAuth, @RequestBody Product product) {
		if(product.getProductName() == null) {
			throw new ApiRequestException("Product should have a product name.");
		}
		
		if(product.getPrice() == 0) {
			throw new ApiRequestException("Product should have a price and it should be greater than zero.");
		}
		return productService.addNewProduct(userIdForAuth, product);
	}
	
	@GetMapping("/{productId}")
	public Product getProduct(@PathVariable int productId) {
		return productService.getProduct(productId);
	}
	
	@PatchMapping("/{productId}")
	public Product updateProduct(@RequestHeader("Authorization") int userIdForAuth, @PathVariable int productId, @RequestBody Product product) {
		if(product.getProductName() == null && product.getPrice() == 0) {
			throw new ApiRequestException("Product should have a product name and a price greater than 0.");
		}
		return productService.updateProduct(userIdForAuth, productId, product);
	}
	
	@DeleteMapping("/{productId}")
	public ResponseEntity<Object> deleteProduct(@RequestHeader("Authorization") int userIdForAuth, @PathVariable int productId) {
		return productService.deleteProduct(userIdForAuth, productId);
	}
}
