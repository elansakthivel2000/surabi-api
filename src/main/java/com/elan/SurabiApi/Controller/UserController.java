package com.elan.SurabiApi.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.elan.SurabiApi.Entity.User;
import com.elan.SurabiApi.Exception.ApiRequestException;
import com.elan.SurabiApi.Service.Interfaces.UserServiceInterface;

/*
 *  User controller has all the routes related to user operation.
 *  	- For request parameters and response body details view API documentation.
 * 
 */

@RestController
@RequestMapping("/api/users")
@CrossOrigin(origins = {"http://localhost:3000", "http://localhost:4200"})
public class UserController {
	
	@Autowired
	private UserServiceInterface userService;
	
	@GetMapping
	public List<User> getAllUsers() {
		return userService.getAllUsers();
	}
	
	@GetMapping("/{userId}")
	public User getUser(@PathVariable int userId) {
		return userService.getUser(userId);
	}
	
	@PatchMapping("/{userId}")
	public User updateUser(@RequestHeader("Authorization") int userIdForAuth, @PathVariable int userId, @RequestBody User user) {
		if(user.getUsername() == null && user.getPassword() == null) {
			throw new ApiRequestException("Username or Password must be passed in the body to update..");
		}
		return userService.updateUser(userIdForAuth, userId, user);
	}
	
	@DeleteMapping("/{userId}")
	public ResponseEntity<Object> deleteUser(@RequestHeader("Authorization") int userIdForAuth, @PathVariable int userId) {
		return userService.deleteUser(userIdForAuth, userId);
	}
	
	@PostMapping("/login")
	public User login(@RequestBody User user) {
		if(user.getUsername() == null || user.getPassword() == null) {
			throw new ApiRequestException("Username and Password is required for login..");
		}
		return userService.login(user);
	}
	
	@PostMapping("/register")
	public User register(@RequestBody User user) throws Exception {
		if(user.getUsername() == null || user.getPassword() == null) {
			throw new ApiRequestException("Username and Password is required..");
		}
		return userService.register(user);
	}
	
	@PostMapping("/logout")
	public ResponseEntity<Object> logout(@RequestHeader("Authorization") int userIdForAuth) {
		if(userIdForAuth == 0) {
			throw new ApiRequestException("User id is required for logout..");
		}
		return userService.logout(userIdForAuth);
	}
}