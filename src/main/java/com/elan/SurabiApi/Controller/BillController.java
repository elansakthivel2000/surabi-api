package com.elan.SurabiApi.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.elan.SurabiApi.Entity.Bill;
import com.elan.SurabiApi.Exception.ApiRequestException;
import com.elan.SurabiApi.Model.Cart;
import com.elan.SurabiApi.Model.Report;
import com.elan.SurabiApi.Service.Interfaces.BillServiceInterface;

/*
 *  Bill controller has all the routes related to bill operation.
 *  	- For request parameters and response body details view API documentation.
 * 
 */

@RestController
@RequestMapping("/api/bills")
@CrossOrigin(origins = {"http://localhost:3000", "http://localhost:4200"})
public class BillController {

	@Autowired
	private BillServiceInterface billService;
	
	@GetMapping
	public List<Bill> getAllBills() {
		return billService.getAllBills();
	}	
	
	@GetMapping("/{billId}")
	public Bill getBill(@PathVariable int billId) {
		return billService.getBill(billId);
	}

	@PostMapping
	public Bill addNewBill(@RequestHeader("Authorization") int userIdForAuth, @RequestBody Cart cart) {
		if(cart == null || cart.getCart().isEmpty()) {
			throw new ApiRequestException("Cart cannot be empty");
		}
	
		return billService.addNewBill(cart, userIdForAuth);
	}
	
	@PatchMapping("/{billId}")
	public Bill updateBillOfUser(
			@RequestHeader("Authorization") int userIdForAuth,
			@PathVariable int billId,
			@RequestBody Bill bill
		) {
		if(bill.getBillAmount() == 0.0 && bill.getNumItems() == 0) {
			throw new ApiRequestException("Please provide a bill amount which is greater than 0,"
					+ "or provide number of items which is greater than 0 to perform update operation");
		}
		
		return billService.updateBill(userIdForAuth, billId, bill);
	}
	
	@DeleteMapping("/{billId}")
	public ResponseEntity<Object> deleteBill(@RequestHeader("Authorization") int userIdForAuth, @PathVariable int billId) {
		return billService.deleteBill(userIdForAuth, billId);
	}
	
	@GetMapping("/today")
	public List<Bill> getAllBillsForToday(@RequestHeader("Authorization") int userIdForAuth) {
		return billService.getAllBillsForToday(userIdForAuth);
	}
	
	@GetMapping("/report/monthly")
	public Report getTotalSalesForMonth(@RequestHeader("Authorization") int userIdForAuth) {
		return billService.getTotalSalesForMonth(userIdForAuth);
	}
}
