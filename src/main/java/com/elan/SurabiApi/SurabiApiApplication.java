package com.elan.SurabiApi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SurabiApiApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(SurabiApiApplication.class, args);
	}

}
