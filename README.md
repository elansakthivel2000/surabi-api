# Surabi API

## Steps to run:-

1. Clone and import it as a maven project.
2. Open MySql Command line tool and run.

   > source <path_to_surabi_sql_file/Surabi.sql>

3. Open src/main/resources application.properties file and change the database url, username and password according to you.
4. Then run the com.elan.SurabiApi.SurabiApiApplication.java file.
5. Open Postman and use the routes.
6. All the route specific details are mentioned in the postman Documentation please refer it.

## API Documentation
Link to Postman Documentation:- [API Documentation](https://documenter.getpostman.com/view/12963762/Tzz8qbf2)